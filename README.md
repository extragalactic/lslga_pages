# make_lslga_pages

A simple script to generate html pages with Legacy Survey cutouts based on the
[LSLGA catalog](https://github.com/moustakas/LSLGA).

More interactive tool for inspection of LSLGA catalog can be found [here](https://github.com/moustakas/lslga-inspect).


## Installation

Setup environment and install dependencies

```
$ python3.7 -m venv ~/.virtualenvs/lslga
$ source ~/.virtualenvs/lslga/bin/activate
(lslga) $ pip install numpy astropy jinja2 argh
```

Download LSLGA catalog

```
$ wget http://www.sos.siena.edu/~jmoustakas/tmp/LSLGA-v2.0.fits
```

## Example

```
(lslga) $ python make_lslga_pages.py --help
usage: make_lslga_pages.py [-h] [-c CHUNK_SIZE] [-z ZLIMIT] file outdir

    Generate bunch of HTML pages with Legacy Survey cutouts based on LSLGA
    catalog.

    Example:

        $ python make_lslga_pages.py LSLGA-v2.0.fits htmls/


positional arguments:
  file                  -
  outdir                -

optional arguments:
  -h, --help            show this help message and exit
  -c CHUNK_SIZE, --chunk-size CHUNK_SIZE
                        1000
  -z ZLIMIT, --zlimit ZLIMIT
                        0.15
```

Generate html files with [Legacy Survey](http://legacysurvey.org/) image cutouts for galaxy subset `z<0.15`

```
(lslga) $ mkdir htmls
(lslga) $ make_lslga_pages.py LSLGA-v2.0.fits htmls/ -z 0.15
```

> **_NOTE:_**  Galaxies are selected with redshifts only.

Open `htmls/main.html` file, click on one of the image chunks and enjoy galaxies!