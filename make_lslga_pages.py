#!/usr/bin/python
from astropy.io import fits
from astropy.cosmology import WMAP9 as cosmo
import numpy as np
import jinja2 as j2
import argh


def main(file, outdir, chunk_size=1000, zlimit=0.15):
    """
    Generate bunch of HTML pages with Legacy Survey cutouts based on LSLGA
    catalog.

    Example:

        $ python make_lslga_pages.py LSLGA-v2.0.fits htmls/
    """

    table_full = fits.getdata(file, 1)

    flt = (table_full['Z'] < zlimit)

    t = table_full[flt]

    env = j2.Environment(loader=j2.FileSystemLoader('.'))
    template_chunk = env.get_template('template_chunk.html')
    template_chunk_kpc = env.get_template('template_chunk_kpc.html')
    template_main = env.get_template('template_main.html')
    template_table = env.get_template('template_table.html')
    template_list = env.get_template('template_list.html')

    ichunks = [k for k in range(t.size)[::chunk_size]]
    ichunks.append(t.size)
    nchunks = len(ichunks) - 1
    data = []

    ndigits = len(str(nchunks))

    for i in range(nchunks):
        print(f"Working on chunk {i}")
        gals = t[ichunks[i]:ichunks[i+1]]
        size60_kpc = 60.0/cosmo.arcsec_per_kpc_proper(gals['Z']).value
        size80_arcsec = cosmo.arcsec_per_kpc_proper(gals['Z']).value*80

        title = f"Chunk {i+1}"

        fmt = "{:0"+str(ndigits)+".0f}"
        idstr = fmt.format(i+1)
        html_file = f"chunk{idstr}.html"
        html_file_80kpc = f"chunk{idstr}_80kpc.html"

        data.append([html_file, html_file_80kpc, idstr, gals])
        ochunk = f"{outdir}/{html_file}"
        ochunk_80kpc = f"{outdir}/{html_file_80kpc}"

        template_chunk.stream(
            gals=zip(gals, size60_kpc), title=title).dump(ochunk)
        template_chunk_kpc.stream(
            gals=zip(gals, size80_arcsec), title=title).dump(ochunk_80kpc)

    omain = f"{outdir}/main.html"
    print(f"Saveing {omain}")
    template_main.stream(data=data, zlimit=zlimit, ngals=t.size).dump(omain)

    otable = f"{outdir}/table.html"
    print(f"Saveing {otable}")
    template_table.stream(data=data, zlimit=zlimit, ngals=t.size).dump(otable)

    olist = f"{outdir}/list.html"
    print(f"Saveing {olist}")
    template_list.stream(data=data, zlimit=zlimit, ngals=t.size).dump(olist)

    print("...done")


argh.dispatch_command(main)
